-- See the file "LICENSE" for the full license governing this code. --
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
use work.can_tp.all;
use work.wishbone.all;
use work.can_tp.all;
use work.config.all;
use work.wb_tp.all;

ENTITY final_TB IS
END ENTITY;

ARCHITECTURE sim OF final_TB IS
	constant MAIN_FREQ		: real := 100.000E6;
	constant CLK_PERIOD 		: time := 1 sec / MAIN_FREQ;
	signal btn		: std_logic_vector(6 downto 0);
	signal sw		: std_logic_vector(7 downto 0);
	signal led		: std_logic_vector(7 downto 0);
	signal timer_irq		: std_logic;
	signal dataLCD		: std_logic_vector(7 downto 0);
	signal busyLCD		: std_logic;
	signal enableLCD		: std_logic;
	signal rsLCD			: std_logic;
	signal rwLCD			: std_logic;
	signal rx_data		: std_logic_vector(7 downto 0);
	signal rx_data_ready :std_logic;
	signal rx_parity_err : std_logic;
	signal tx_ack_err    : std_logic;		
	signal ps2_clk	: std_logic := 'H';
	signal ps2_data	: std_logic := 'H';
	signal tx_o			: std_logic;
	signal rx_i			: std_logic;
	-- management signal
	signal clk : std_logic := '0';
	signal rst_gen : std_logic := '1';

	signal fake_interrupt : std_logic := '0';
	
	COMPONENT lt16soc_top IS
		generic(
			programfilename : string := "programs/finalproject.ram"
		);
		port(
			-- clock signal
			clk		: in  std_logic;
			-- external reset button
			rst		: in std_logic;
			-- external LEDs
			led		: out std_logic_vector(7 downto 0);
			-- external swtich buttons
			btn		: in std_logic_vector(6 downto 0);
			sw			: in std_logic_vector(7 downto 0);
			-- Timer
			timer_irq : out std_logic;
			-- LCD
			dataLCD		: inout std_logic_vector(7 downto 0);
			busyLCD     : out std_logic;
			enableLCD	: out std_logic;
			rsLCD		: out std_logic;
			rwLCD		: out std_logic;
			-- RX
			rx_data			: out std_logic_vector(7 downto 0);
			rx_data_ready	: out std_logic;
			rx_parity_err	: out std_logic;
			-- TX
			tx_ack_err		: out std_logic;
			-- Keyboard
			ps2_keyb_clk	: inout std_logic;
			ps2_keyb_data	: inout std_logic;
			-- CAN controller NOTE: CHECK top.ucf for these signals
			can_tx_pin		: out std_logic;
			can_rx_pin		: in std_logic;
			trigger_int		: in std_logic
		);
	END COMPONENT;
	
BEGIN			
	-- clock generation
	clk_gen: process
	begin
		clk	<= not clk;
		wait for CLK_PERIOD/2;
	end process clk_gen;

	dut: lt16soc_top port map(
		clk 			=> clk,
		rst 			=> rst_gen,
		led 			=> led,
		btn 			=> btn,
		sw 			=> sw,
		timer_irq 	=> timer_irq,
		dataLCD		=> dataLCD,
		busyLCD		=> busyLCD,
		enableLCD	=> enableLCD,
		rsLCD			=> rsLCD,
		rwLCD			=> rwLCD,
		rx_data		=> rx_data,
		rx_data_ready => rx_data_ready,
		rx_parity_err	=> rx_parity_err,
		tx_ack_err		=> tx_ack_err,
		ps2_keyb_clk	=> ps2_clk,
		ps2_keyb_data	=> ps2_data,
		can_tx_pin		=> tx_o,
		can_rx_pin		=> rx_i,
		trigger_int 	=> fake_interrupt
	);
	-- stimuli
	stimuli: process is
	begin
		report "begin stimuli" severity warning;
		rst_gen <= '0';
		
		wait for 40 ns;
			rst_gen <= '1';
		wait for 3000 ns;
		-- simulate CAN RX
		fake_interrupt <= '1';
		wait for 10 ns;
		fake_interrupt <= '0';
		wait for 10 ms;
		report "end stimuli" severity failure;
		wait;
	
	end process stimuli;


END ARCHITECTURE;
