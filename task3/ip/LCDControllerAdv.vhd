-- See the file "LICENSE" for the full license governing this code. --

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE work.lt16x32_global.all;
USE work.wishbone.all;
USE work.config.all;

ENTITY wb_lcd IS
	generic(
		memaddr		:	generic_addr_type := CFG_BADR_LCD;
		addrmask		:	generic_mask_type := CFG_MADR_LCD
	);
	port(
		clk			: in std_logic;
		rst			: in std_logic;

		dataLCD		: inout std_logic_vector(7 downto 0);
		initLCD		: out std_logic;
		busyLCD		: out std_logic;
		enableLCD	: out std_logic;
		rsLCD			: out std_logic;
		rwLCD			: out std_logic;

		wslvi		: in wb_slv_in_type;
		wslvo		: out wb_slv_out_type
	);
END ENTITY;

ARCHITECTURE Behavioral OF wb_lcd IS

	signal lcd_reg	: std_logic_vector(15 downto 0);
	signal ack		: std_logic;
	signal en		: std_logic;
	-- Bit fields
	constant BIT_INIT  : integer := 12;
	constant BIT_BUSY	 : integer := 11;
	constant BIT_EN    : integer := 10;
	constant BIT_RS    : integer := 9;
	constant BIT_RW    : integer := 8;
	-- PowerUp, Init, Waiting, Sending
   type CONTROL is(P, I, W, S);
   signal STATE       : CONTROL;
BEGIN
	process(clk)
	variable clk_count  : integer := 0;   -- counter for wait
	variable INIT_STATE : std_logic_vector(2 downto 0) := "000";
	begin
		if clk'event and clk='1' then
			if rst = '1' then
				ack		<= '0';
				clk_count := 0;
				lcd_reg	<= (others => '0');
				lcd_reg(BIT_INIT) <= '1';
				STATE		<= P;
			else
				case STATE is
				  --wait 50 ms
					when P =>
						-- Indiciates initilization is not done
						lcd_reg(BIT_INIT) <= '1';
						lcd_reg(BIT_BUSY) <= '1';
						--wait 50 ms
						if(clk_count <  5000000) then
							clk_count 	:= clk_count + 1;
							STATE 		<= P;
						else
							clk_count := 1;
							STATE <= I;
						end if;
					--cycle through initialization sequence  
					when I =>
						clk_count := clk_count + 1;
						-- function set
						-- 8-bit interface, 2 lines, 5x8 dot chars
						if (INIT_STATE = "000") then
							lcd_reg(7 downto 0) <= x"38";
							lcd_reg(BIT_EN) <= '1';
							lcd_reg(BIT_RS) <= '0';
							lcd_reg(BIT_RW) <= '0';
							clk_count := 1;
							INIT_STATE := "001";
						--wait
						elsif(INIT_STATE = "001") then
							-- 10us wait for E clr
							if(clk_count >= 1000) then
								lcd_reg(BIT_EN) <= '0';
							end if;
							-- 40 us wait
							if(clk_count >= 4000) then
								INIT_STATE := "010";
							end if;
						--display on, cursor on, blink on
						elsif(INIT_STATE = "010") then
							lcd_reg(7 downto 0) <= x"0F";          
							lcd_reg(BIT_EN) <= '1';
							lcd_reg(BIT_RS) <= '0';
							lcd_reg(BIT_RW) <= '0';
							clk_count := 1;
							INIT_STATE := "011";
						-- wait
						elsif(INIT_STATE = "011") then
							-- 10us wait for E clr
							if(clk_count >= 1000) then
								lcd_reg(BIT_EN) <= '0';
							end if;
							-- 40us
							if(clk_count >= 4000) then
								INIT_STATE := "100";
							end if;
						-- clear lcd
						elsif(INIT_STATE = "100") then
							lcd_reg(7 downto 0) <= x"01";
							lcd_reg(BIT_EN) <= '1';
							clk_count := 1;
							INIT_STATE := "101";
						-- wait
						elsif(INIT_STATE = "101") then
							-- 10us wait for E clr
							if(clk_count >= 1000) then
								lcd_reg(BIT_EN) <= '0';
							end if;
							-- 2ms wait
							if(clk_count >= 200000) then
								INIT_STATE := "110";
							end if;
						-- entry mode: increment mode, entire shift off
						elsif(INIT_STATE = "110") then
							lcd_reg(7 downto 0) <= x"06";
							lcd_reg(BIT_EN) <= '1';
							lcd_reg(BIT_RS) <= '0';
							lcd_reg(BIT_RW) <= '0';
							clk_count := 1;
							INIT_STATE := "111";
						-- wait
						elsif(INIT_STATE = "111") then
							-- 10us wait for E clr
							if(clk_count >= 1000) then
								lcd_reg(BIT_EN) <= '0';
							end if;
							-- 2ms wait
							if(clk_count >= 200000) then
								INIT_STATE := "000";
								--initialization complete
								STATE <= W;
							end if;
						end if;
					--wait for the enable signal and then latch in the instruction
					when W =>
						lcd_reg(BIT_INIT) <= '0';
						if wslvi.stb = '1' and wslvi.cyc = '1' then
							if wslvi.we = '1' then
								lcd_reg(10 downto 0)	<= dec_wb_dat(wslvi.sel,wslvi.dat)(10 downto 0);
							end if;							
							if ack = '0' then
								ack		<= '1';
							else
								ack		<= '0';
							end if;
						else
							ack			<= '0';
						end if;
						-- software wants to write (ENABLE == '1')
						if(lcd_reg(BIT_EN) ='1') then
							lcd_reg(BIT_BUSY) <= '1';
							STATE <= S;
						else
							clk_count := 1;
							STATE <= W;
						end if;
					--send instruction to lcd        
					when S =>
						-- 10us wait for E clr
						if(clk_count >= 1000) then
							lcd_reg(BIT_EN) <= '0';
						end if;
						--wait for 1ms
						if(clk_count < 100000) then 
							clk_count := clk_count + 1;
							STATE <= S;
						else
							clk_count := 1;
							lcd_reg(BIT_BUSY) <= '0';
							STATE <= W;
						end if;
				end case;
			end if;
		end if;
	end process;
	--wslvo.dat(15 downto 0)	<= lcd_reg when wslvi.adr(2) = '0'
	--	else "000" & dataLCD when wslvi.adr(2) = '1' and lcd_reg(BIT_RW) = '1'
	--	else (others => '0');

	wslvo.dat(31 downto 16)	<= (others=>'0');
	wslvo.dat(BIT_INIT) <= lcd_reg(BIT_INIT);
	wslvo.dat(BIT_BUSY) <= lcd_reg(BIT_BUSY);
	wslvo.dat(BIT_EN) <= lcd_reg(BIT_EN);
	wslvo.dat(BIT_RS) <= lcd_reg(BIT_RS);
	wslvo.dat(BIT_RW) <= lcd_reg(BIT_RW);
	wslvo.dat(7 downto 0) <= lcd_reg(7 downto 0);
	
	wslvo.wbcfg				<= wb_membar(memaddr, addrmask);
	wslvo.ack				<= ack;
	
	dataLCD		<= lcd_reg(7 downto 0) when lcd_reg(BIT_RW) = '0' else "ZZZZZZZZ";
	initLCD		<= lcd_reg(BIT_INIT);
	busyLCD		<= lcd_reg(BIT_BUSY);
	enableLCD	<= lcd_reg(BIT_EN);
	rsLCD			<= lcd_reg(BIT_RS);
	rwLCD			<= lcd_reg(BIT_RW);
END ARCHITECTURE;
