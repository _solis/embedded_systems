-- See the file "LICENSE" for the full license governing this code. --
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;
USE work.lt16soc_peripherals.ALL;
USE work.wishbone.ALL;
USE work.wb_tp.ALL;
USE work.config.ALL;

ENTITY slvtest_lcd IS
END ENTITY;

ARCHITECTURE sim OF slvtest_lcd IS

	constant CLK_PERIOD : time := 4 ns;

	signal sim_clk 		: std_logic := '0';
	signal sim_rst			: std_logic;
	signal sim_data		: std_logic_vector(7 downto 0);
	signal sim_init		: std_logic;
	signal sim_busy		: std_logic;
	signal sim_enable		: std_logic;
	signal sim_rs			: std_logic;
	signal sim_rw			: std_logic;
   signal slvi			: wb_slv_in_type;
	signal slvo			: wb_slv_out_type;
	 -- Bits
	 --	EN[10], RS[9], RW[8], DATA[7:0]
    constant BIT_RW  : integer := 8; -- Read/Write
    constant BIT_RS  : integer := 9; --  Register Select
    constant BIT_EN  : integer := 10; --  Enable/Disable
	 signal var 		: std_logic_vector(31 downto 0); -- temporary variable
BEGIN

	SIM_SLV: wb_lcd
		generic map(
			memaddr		=> CFG_BADR_LCD,
			addrmask		=> CFG_MADR_LCD
		)
		port map(
			clk			=> sim_clk,
			rst			=> sim_rst,
			dataLCD		=> sim_data,
			initLCD		=> sim_init,
			busyLCD		=> sim_busy,
			enableLCD	=> sim_enable,
			rsLCD			=> sim_rs,
			rwLCD			=> sim_rw,
			wslvi			=> slvi,
			wslvo			=> slvo
		);

	clk_gen: process
	begin
		sim_clk	<= not sim_clk;
		wait for CLK_PERIOD/2;
	end process clk_gen;

	stimuli: process
	begin
		sim_rst	<= '1';
		var <= x"FACEFACE";
		-- Start
		wait for CLK_PERIOD;
			sim_rst	<= '0';
			var <= x"00000000";
			
		-- Wait for init to be done 
		wait on sim_init until sim_init = '0';
			var <= x"00000001";
		
		-- Wait for controller not to be busy
		if (sim_busy = '1') then
			wait on sim_busy until sim_busy = '0';
		end if;
		var <= x"00000002";
		
		-- Send the letter 'A'
		wait for CLK_PERIOD;
			var <= x"00000441";
			generate_sync_wb_single_write(slvi,slvo,sim_clk,var,"01",0);
		
		-- wait until busy flag is gone
		if (sim_busy = '1') then
			wait on sim_busy until sim_busy = '0';
		end if;
		var <= x"00000003";
		
		-- END
		wait for CLK_PERIOD*1000000;		
			var <= x"DEADDEAD";
		wait;
	end process stimuli;

END ARCHITECTURE;

