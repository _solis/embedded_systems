-- See the file "LICENSE" for the full license governing this code. --
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY warmup3_tb IS
END ENTITY;

ARCHITECTURE sim OF warmup3_tb IS

	constant CLK_PERIOD : time := 4 ns;

	signal sim_clk 	: std_logic := '0';
	signal sim_rst		: std_logic;
	signal sim_bts		: std_logic_vector(6 downto 0);
	signal sim_sws		: std_logic_vector(7 downto 0);
	signal sim_led		: std_logic_vector(7 downto 0);
	signal sim_irq		: std_logic;
	signal sim_dataLCD		: std_logic_vector(7 downto 0);
	signal sim_initLCD		: std_logic;
	signal sim_busyLCD		: std_logic;
	signal sim_enableLCD		: std_logic;
	signal sim_rsLCD			: std_logic;
	signal sim_rwLCD			: std_logic;
			
	COMPONENT lt16soc_top IS
		generic(
			programfilename : string := "programs/assignment32code.ram"
		);
		port(
			-- clock signal
			clk		: in  std_logic;
			-- external reset button
			rst		: in std_logic;
			-- external LEDs
			led		: out std_logic_vector(7 downto 0);
			-- external swtich buttons
			btn		: in std_logic_vector(6 downto 0);
			sw			: in std_logic_vector(7 downto 0);
			-- Timer
			timer_irq : out std_logic;
			-- LCD
			dataLCD		: inout std_logic_vector(7 downto 0);
			initLCD     : out std_logic;
			busyLCD     : out std_logic;
			enableLCD	: out std_logic;
			rsLCD		: out std_logic;
			rwLCD		: out std_logic
		);
	END COMPONENT;

BEGIN

	dut: lt16soc_top port map(
		clk=>sim_clk,
		rst=>sim_rst,
		led=>sim_led,
		btn=>sim_bts,
		sw=>sim_sws,
		timer_irq=>sim_irq,
		dataLCD=>sim_dataLCD,
		initLCD=>sim_initLCD,
		busyLCD=>sim_busyLCD,
		enableLCD=>sim_enableLCD,
		rsLCD=>sim_rsLCD,
		rwLCD=>sim_rwLCD
	);

	clk_gen: process
	begin
		sim_clk	<= not sim_clk;
		wait for CLK_PERIOD/2;
	end process clk_gen;

	stimuli: process
	begin
		sim_rst	<= '0';
		wait for CLK_PERIOD;
		sim_rst	<= '1';
		sim_bts	<= "0000001";
		sim_sws	<= "00000001";
		
		wait for CLK_PERIOD*10;
		sim_bts	<= "0000010";
		sim_sws	<= "00000010";
		
		wait for CLK_PERIOD*10;
		sim_bts	<= "0000011";
		sim_sws	<= "00000011";
		
		wait for CLK_PERIOD*10;
		sim_bts	<= "1000000";
		sim_sws	<= "10000000";
	
		wait for 10000000 ns;
		assert false report "Simulation terminated!" severity failure;
	end process stimuli;


END ARCHITECTURE;
