//    Assignment 3,2
reset:
    br always >main
    nop
hardfault:
    clr r3
    addi r3, 0x19
    st08 r1, r3
    reti
    nop
memfault:
    clr r3
    addi r3, 0x66
    st08 r1, r3
    reti
    nop
timer_irq:
    br >timer_handler
    nop
// Timer IRQ handler
timer_handler:
    reti
    nop
// LCD Register:
// [12] [11] [10] [9] [8] [7:0]
// Init Busy EN   RS  R/W DATA
.align
    LED_addr:   .word 0x000F0000
    LCD_addr:   .word 0x000F0010
    sp_init:    .word 0x00000400

// For simulation
//.align
//    delay_2ms:  .word   0x00000001  // 1ms
    
// For implementation
.align
    delay_2ms:  .word   0x00020000  // 2ms

// 48 61 72 64 77 61 72 65 20 72 75 6c 7a 21
.align
ascii_table:
	.word 0x48 //H
	.word 0x61 //a
	.word 0x72 //r
	.word 0x64 //d
	.word 0x77 //w
	.word 0x61 //a
	.word 0x72 //r
	.word 0x65 //e
	
	.word 0x20 //space
	
	.word 0x72 //r
    .word 0x75 //u
    .word 0x6C //l
	.word 0x7A //z
	
	.word 0x21 //!
.align
flag_table:
    .word 0x00000200    // RS_1
    .word 0xFFFFFDFF    // RS_0
    .word 0x00000100    // RW_1
    .word 0xFFFFFEFF    // RW_0
    .word 0x00000400    // EN_1
    .word 0xFFFFFBFF    // EN_0
    .word 0x00000080    // DB_1
.align
p_ascii_table:
    .word=ascii_table
.align
p_flag_table:
    .word=flag_table

// General: r0 - r11
// Special: r12 (SP), r13 (LR), r14 (SR), r15 (PC)
//Register definitions:
//    r0 = LCD base address
//    r1 = LED base address
//    r2 = data to/from LCD
//    r3 = data to LED
//    r4 = data to check flags
//    r5 = ptr to ascii table
//    r6 = free
//    r7 = ptr to flag table
//    r8 = delay we want
//    r9 = free
//    r10 = delay tmp
//    r11 = subroutine temp

// Setup
main:
    // Clear all registers first
    clr r0
    clr r1
    clr r2
    clr r3
    clr r4
    clr r5
    clr r6
    clr r7
    clr r8
    clr r9
    clr r10
    clr r11
    
    // initiate stack pointer
    ldr sp, >sp_init
    // Assign addresses
    ldr r0,>LCD_addr
    ldr r1,>LED_addr
    ldr r5,>p_ascii_table
    ldr r7,>p_flag_table
    ldr r8, >delay_2ms
    // Clear LEDs
    st08 r1, r3
    
    // After power on, poll flag to check init is done, init deassert
    // Init flag in 12th bit
    addi r11, 1
    wait_for_init:
        ld16 r4, r0
        rsh r4, r4, 12
        cmp eq r4, r11
        br true >wait_for_init
        nop
    
    // LEDs: 0101 0101
    addi r3, 85
    st08 r1, r3
    
    // Write "Hardware rulz!"
    call >lcd_write
    nop
    
    lsh r8, r8, 7
    
loop_forever:
    clr r3
    st08 r1, r3
    call >delay
    
    addi r3, 0xF
    lsh r3, r3, 4
    addi r3, 0xF
    st08 r1, r3
    call >delay
	    
    br >loop_forever
    nop
        
// Pseudo delay function: Increments r10 and r8 (delay value)
delay:
    clr r10
	continue:
	    cmp neq r10,r8
	    br true >continue
	    addi r10,1

	ret
	clr r10

// Waits for the controller's busy flag[11] to clear
busy_wait:
    clr r11
    addi r11, 1
    wait_deassert:
        ld16 r4, r0
        rsh r4, r4, 11
        cmp eq r4, r11
        br true >wait_deassert
        nop
        
    ret
    clr r11
    
lcd_write:
    // push LR onto stack
    addi sp, -4
    st32 sp, lr
    
    // Write 
    // ASCII: Assembler rocks!
    clr r2
    // "H"
    ld32 r2, r5
    call >send_data
    addi r5, 4
    
    // "a"
    ld32 r2, r5
    call >send_data
    addi r5, 4
    
    // "r"
    ld32 r2, r5
    call >send_data
    addi r5, 4
    
    // "d"
    ld32 r2, r5
    call >send_data
    addi r5, 4
    
    // "w"
    ld32 r2, r5
    call >send_data
    addi r5, 4
    
    // "a"  
    ld32 r2, r5
    call >send_data
    addi r5, 4
    
    // "r"
    ld32 r2, r5
    call >send_data
    addi r5, 4
    
    // "e"
    ld32 r2, r5
    call >send_data
    addi r5, 4
    
    // " "
    ld32 r2, r5
    call >send_data
    addi r5, 4
    
    // "r"
    ld32 r2, r5
    call >send_data
    addi r5, 4
    
    // "u"
    clr r2
    ld32 r2, r5
    call >send_data
    addi r5, 4
    
    // "l"
    ld32 r2, r5
    call >send_data
    addi r5, 4
    
    // "z"
    ld32 r2, r5
    call >send_data
    addi r5, 4
 
    // "!"
    ld32 r2, r5
    call >send_data
    addi r5, -52
          
    clr r2
    addi r2, 0x40 
    // change DDRAM address
    addi r7, 24
    ld32 r11, r7
    or r2, r2, r11
    addi r7, -24
    call >send_command
    
    clr r2
    addi r2, 0x23
    call >send_data
    nop
    clr r2
    addi r2, 0x59 
    call >send_data
    nop
    clr r2
    addi r2, 0x4F
    call >send_data
    nop
    clr r2
    addi r2, 0x4C
    call >send_data
    nop
    clr r2
    addi r2, 0x4F
    call >send_data
    nop
    
    // Pop link address back
	ld32 lr, sp
	addi sp, 4
	
	ret
	nop

// Subroutine to send data
//flag_table:
//    .word 0x00000200    // RS_1
//    .word 0xFFFFFDFF    // RS_0
//    .word 0x00000100    // RW_1
//    .word 0xFFFFFEFF    // RW_0
//    .word 0x00000400    // EN_1
//    .word 0xFFFFFBFF    // EN_0
send_data:
    // push LR onto stack
    addi sp, -4
    st32 sp, lr
    
    // Make sure controller is not busy
    call >busy_wait
    nop
    
    // RS set
    ld32 r11, r7
    or r2, r2, r11
    // RW clr
    addi r7, 12
    ld32 r11, r7
    and r2, r2, r11
    // E set
    addi r7, 4
    ld32 r11, r7
    or r2, r2, r11
    // Reset pointer
    addi r7, -16
    
    // Send command
    st16 r0, r2
    
    // Wait for busy flag to clr
    call >busy_wait
    nop
    
    // Pop link address back
	ld32 lr, sp
	addi sp, 4
	
	ret
	clr r11
	
send_command:
    // push LR onto stack
    addi sp, -4
    st32 sp, lr
    
    // Make sure controller is not busy
    call >busy_wait
    nop
    
    // RS clr
    addi r7, 4
    ld32 r11, r7
    and r2, r2, r11
    // RW clr
    addi r7, 8
    ld32 r11, r7
    and r2, r2, r11
    // E set
    addi r7, 4
    ld32 r11, r7
    or r2, r2, r11
    
    // Reset pointer
    addi r7, -16
    
    // Send command
    st16 r0, r2
    
    // Wait for busy flag to clr
    call >busy_wait
    nop
    
    // Pop link address back
	ld32 lr, sp
	addi sp, 4
	
	ret
	clr r11	
