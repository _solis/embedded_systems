# Embedded Systems

All VHDL code used for Embedded Systems Lab all written for the Genesys Board based on Xilinx Virtex 5 LX50T. Uses Wishbone B4 System-on-Chip (SoC)Interconnection
Architecturefor Portable IP Cores as the main bus interface. The core is a university proprietary, and thus, not part of this repo. Due to this, some information was left out. This repository only contains code written by me. All code compiled and tested on [Xilinx's ISE Design Studio](https://www.xilinx.com/products/design-tools/ise-design-suite.html)

** Folder Structure **
```
├ docs                  # Resource documents for Xilinx board
├── task[number]        # Main folder containing each task
├── ip                  # VHDL code for written IP modules
├── programs            # Assemlby code to test IP
├── testbench           # Test bench code for individual IP simulation and final synthesis
└── README.md
```
## Task 2
Learning Outcomes:
- Implement IP with VHDL
- Integrate IPs to SoC as slave interfaces and integrate interrupts to timer IP
- Write a testbench and individually test each IP, then to test it integrated to the SoC
- Write assembly code to interact with the IPs from the processor
- Write an ISR
- Verify on FPGA

### Task 2.1 - 2.3
- Design an IP to interact with Genesys' 8 on-board switches as 8-bits
- Implemention of the IP as a wishbone slave
- Test individual IP by writing a testbench and simulation. Validate waveforms
- Integrate IP to SoC and verify synthesis without errors
- Write assembly code to turn on and off the on-board LEDs with a delay based on the generated value from the 8-bit switch
- Simulate and validate IP by capturing waveforms
- Synthesize and download to Genesys Board for full test

### Task 2.4
- Design an IP to count from 0 to a given value. IP must have 2, 32-bit registers (Target Count and Control)
- Implemention of the IP as a wishbone slave
- Test individual IP by writing a testbench and simulation. Validate waveforms
- Integrate IP to SoC and verify synthesis without errors
- Write assembly code to turn on and off the on-board LEDs by the use of an ISR that is triggered when Target Count has been reached
- Simulate and validate IP by capturing waveforms
- Synthesize and download to Genesys Board for full test

## Task 3
Learning Outcomes:
- Timing requiremnets for LCD
- Programming LCD
- Initially use assembly software to interact with LCD while following timing requirements to print a message (Task 3.1)
- Design an IP that will handle all timing requirements and act as a "middleman" between processor and LCD. Use previously assembly software as guide for VHDL code (Task 3.2)
- Integrate, test, and verify

### Task 3.1
- Read Genesys Board manual LCD section. Starts from page 18 and the ST7066 controller datasheet
- Integrate Genesys Board's LCD into system by mapping interface to FPGA pins, add a base address for it, and add it to the top level entity's port list (not in repo)
- Write assembly to send a command and another to send data while taking LCD timing requirements into consideration. A rough implementatino of timing will suffice.
- Write assembly program to initialize LCD. Using this routine and the previous two, display "Assembler rocks!"

### Task 3.2
- Similar to Task 3.1, but instead in VHDL. Write code to act as the "middleman" between the CPU sending/reading data to the LCD and the LCD.
- IP should handle all the timing requiremnts. It should also have 2 read only flags, one for initiliaztion complete and another for LCD controller busy for whenever the IP is busy with a command/data.
- Simulate and verify that the CPU waits for the initilization flag before sending any commands or data, verify that the CPU waits for the busy LCD controller's busy flag (this can be done through polling), and verify that the CPU can write "Hardware rulz!" to the LCD.

## Task 4
Learning Outcomes:
- Interface with a more comlex peripheral device, PS/2 keyboard
- Study standardized protocol specifications
- Implement software and hardware to interact with peripheral
- Design a driver for the PS/2 keyboard
### Task 4.1
- Study the PS/2 protocol
- Implement a IP submodule to receive incoming PS/2 keyboard scan codes and to transmit to the PS/2 keyboard
- Implement PS/2 keyboard to ASCII in hardware (optional - done)
- Add interrupt for data receive
### Task 4.2
- Develop the assembly driver to receive PS/2 scan codes
- Translate scan codes to ASCII (if not done by the hardware)
- Service data received interrupt and display received characters to LCD
- Implement left and right arrow keys (optional - done)

## Semester Design Project
Built on top of previously designed hardware/software, design a 'client' in a distributed system.

Distributed System:
- Connects clients via CAN protocol

Client:
- Synchronize data between all clients via CAN
- Ability to modify data via PS/2 keyboard
- Display synced data onto LCD
- 16 bytes of data (ASCII) shared by the system. This data represents the first row of every client's LCD
- Capable of simultaneously listening on the CAN bus while sending an update.

Provided material:
- CAN controller written in Verilog with 8-bit Wishbone interface. 
- Wrapper to CAN controller written in VHDL
- Adapter board to connect FPGA client's CAN bus to the rest of the system via RS485. Connection from FPGA to adapter is via PMOD

### Task
- Integrate CAN wrapper IP to SoC
- Add appropriate I/O signals to top-level entity for PMOD interface.
- Map connector data pins to ports in top-level entity. View pg.27 of Gensis data sheet for pins.
- Write driver in assembly for above client.

