-- See the file "LICENSE" for the full license governing this code. --
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;
USE work.lt16soc_peripherals.ALL;
USE work.wishbone.ALL;
USE work.wb_tp.ALL;
USE work.config.ALL;

ENTITY slvtest_timer IS
END ENTITY;

ARCHITECTURE sim OF slvtest_timer IS

	constant CLK_PERIOD : time := 10 ns;

	signal sim_clk 		: std_logic := '0';
	signal sim_rst			: std_logic;
	signal sim_irq 	: std_logic;
   signal slvi	: wb_slv_in_type;
	signal slvo	: wb_slv_out_type;

	 -- Bits, [2]: Reset, [1]: Repeat, [0]: Enable
    constant BIT_ENB    : integer := 0; -- 0/1 disable/enable
    constant BIT_REP    : integer := 1; -- 0/1 no/yes repeat
    constant BIT_RST    : integer := 2; -- 0/1
	 constant TARGET		: integer := 5; -- Target counter for timer
	 signal var : std_logic_vector(31 downto 0); -- temporary variable
BEGIN
	SIM_SLV: wb_timer
		generic map(
			memaddr		=> CFG_BADR_TIMER,
			addrmask		=> CFG_MADR_TIMER
		)
		port map(
			clk		=> sim_clk,
			rst		=> sim_rst,
			timer_irq 	=> sim_irq,
			wslvi		=> slvi,
			wslvo		=> slvo
		);

	clk_gen: process
	begin
		sim_clk	<= not sim_clk;
		wait for CLK_PERIOD/2;
	end process clk_gen;

	stimuli: process
	begin
		sim_rst	<= '1';
		var <= x"00000000";
		wait for CLK_PERIOD;
		-- Start
			sim_rst	<= '0';
			var <= x"FACEFACE";
			
		wait for CLK_PERIOD;
			-- Write the target counter value
			var <= std_logic_vector(to_unsigned(TARGET,32));
			generate_sync_wb_single_write(slvi,slvo,sim_clk,var,"10",0);
			var <= x"00000000";
		wait for CLK_PERIOD;
		-- Read counter
			generate_sync_wb_single_read(slvi,slvo,sim_clk,var,0,"10");
			var <= x"00000000";	
			
-- Test no repeat and enable shutoff
		wait for CLK_PERIOD;
			var <= x"11111111";
		wait for CLK_PERIOD;
			var(31 downto 3) <= (others => '0');
			var(BIT_ENB) <= '1';
			var(BIT_REP) <= '0';
			var(BIT_RST) <= '0';
		wait for CLK_PERIOD;
			generate_sync_wb_single_write(slvi,slvo,sim_clk,var,"10",4);
		wait for CLK_PERIOD*12;
		-- Read control registers, should be 0
			generate_sync_wb_single_read(slvi,slvo,sim_clk,var,4,"10");
		wait for CLK_PERIOD;
			var <= x"111111EE";
-- end test
			
-- Reset timer
		wait for CLK_PERIOD;
			var <= x"1111AAAA";
		wait for CLK_PERIOD;
			var(31 downto 3) <= (others => '0');
			var(BIT_ENB) <= '0';
			var(BIT_REP) <= '0';
			var(BIT_RST) <= '1';
		wait for CLK_PERIOD;
			generate_sync_wb_single_write(slvi,slvo,sim_clk,var,"10",4);
		wait for CLK_PERIOD;
			var <= x"1111AAEE";
-- end reset

-- Test repeat
		wait for CLK_PERIOD;
			var <= x"22222222";
		wait for CLK_PERIOD;
			var(31 downto 3) <= (others => '0');
			var(BIT_ENB) <= '1';
			var(BIT_REP) <= '1';
			var(BIT_RST) <= '0';
		wait for CLK_PERIOD;
			generate_sync_wb_single_write(slvi,slvo,sim_clk,var,"10",4);
		wait for CLK_PERIOD*22;
		-- Read control registers, should be 0x0000_0003
			generate_sync_wb_single_read(slvi,slvo,sim_clk,var,4,"10");
		wait for CLK_PERIOD;
			var <= x"222222EE";
-- end test

-- Reset timer
		wait for CLK_PERIOD;
			var <= x"1111BBBB";
		wait for CLK_PERIOD;
			var(31 downto 3) <= (others => '0');
			var(BIT_ENB) <= '0';
			var(BIT_REP) <= '0';
			var(BIT_RST) <= '1';
		wait for CLK_PERIOD;
			generate_sync_wb_single_write(slvi,slvo,sim_clk,var,"10",4);
		wait for CLK_PERIOD;
			var <= x"1111BBEE";
-- end reset

-- Test reset
		wait for CLK_PERIOD;
			var <= x"33333333";
		wait for CLK_PERIOD;
			var(31 downto 3) <= (others => '0');
			var(BIT_ENB) <= '1';
			var(BIT_REP) <= '1';
			var(BIT_RST) <= '0';
			generate_sync_wb_single_write(slvi,slvo,sim_clk,var,"10",4);
		wait for CLK_PERIOD*19;
		-- Write reset
			var(31 downto 3) <= (others => '0');
			var(BIT_ENB) <= '1';
			var(BIT_REP) <= '1';
			var(BIT_RST) <= '1';
			generate_sync_wb_single_write(slvi,slvo,sim_clk,var,"10",4);
		wait for CLK_PERIOD;
			var <= x"333333EE";
-- end test

-- Reset timer
		wait for CLK_PERIOD;
			var <= x"1111CCCC";
		wait for CLK_PERIOD;
			var(31 downto 3) <= (others => '0');
			var(BIT_ENB) <= '0';
			var(BIT_REP) <= '0';
			var(BIT_RST) <= '1';
		wait for CLK_PERIOD;
			generate_sync_wb_single_write(slvi,slvo,sim_clk,var,"10",4);
		wait for CLK_PERIOD;
			var <= x"1111CCEE";
-- end reset

		wait for CLK_PERIOD;
		-- END
			var <= x"DEADDEAD";
		wait;
	end process stimuli;

END ARCHITECTURE;

