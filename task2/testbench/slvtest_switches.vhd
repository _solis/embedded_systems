-- See the file "LICENSE" for the full license governing this code. --
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;
USE work.lt16soc_peripherals.ALL;
USE work.wishbone.ALL;
USE work.wb_tp.ALL;
USE work.config.ALL;

ENTITY slvtest_switches IS
END ENTITY;

ARCHITECTURE sim OF slvtest_switches IS

	constant CLK_PERIOD : time := 4 ns;

	signal sim_clk 	: std_logic := '0';
	signal sim_rst		: std_logic;
	signal sim_bts		: std_logic_vector(6 downto 0);
	signal sim_sws		: std_logic_vector(7 downto 0);
	signal slvi	: wb_slv_in_type;
	signal slvo	: wb_slv_out_type;

	signal stats_byte : std_logic_vector(7 downto 0);
	signal stats_hfwd: std_logic_vector(15 downto 0);
	signal stats_word : std_logic_vector(31 downto 0);
BEGIN

	SIM_SLV: wb_switches
		generic map(
			memaddr		=> CFG_BADR_SWITCHES,
			addrmask		=> CFG_MADR_SWITCHES
		)
		port map(
			clk		=> sim_clk,
			rst		=> sim_rst,
			bts		=> sim_bts,
			sws		=> sim_sws,
			wslvi	=> slvi,
			wslvo	=> slvo
		);

	clk_gen: process
	begin
		sim_clk	<= not sim_clk;
		wait for CLK_PERIOD/2;
	end process clk_gen;

	stimuli: process
	begin
		sim_rst	<= '1';
		wait for CLK_PERIOD;
		sim_rst	<= '0';
		-- Assign inital values
		sim_bts	<= "1000001";
		sim_sws	<= "10000001";
		stats_byte <= x"00";
		stats_hfwd <= x"0000";
		stats_word <= x"00000000";
		
		-- Read single byte, half word, word after CLK_PERIOD(x3)
		wait for CLK_PERIOD;
		stats_byte <= x"55";
		generate_sync_wb_single_read(slvi,slvo,sim_clk,stats_word,0,"00");
		stats_byte <= stats_word(7 downto 0);
		
		wait for CLK_PERIOD;
		sim_bts	<= "1100011";
		sim_sws	<= "11000011";
		stats_hfwd <= x"AAAA";
		generate_sync_wb_single_read(slvi,slvo,sim_clk,stats_word,0,"01");
		stats_hfwd <= stats_word(15 downto 0);
		
		wait for CLK_PERIOD;
		sim_bts	<= "1110111";
		sim_sws	<= "11100111";
		generate_sync_wb_single_read(slvi,slvo,sim_clk,stats_word,0,"10");
		
		-- Finish
		wait for CLK_PERIOD;
		stats_byte <= x"00";
		stats_hfwd <= x"0000";
		stats_word <= x"00000000";
		sim_bts	<= "1111111";
		sim_sws	<= "11111111";
		wait;
	end process stimuli;

END ARCHITECTURE;
