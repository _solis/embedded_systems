-- See the file "LICENSE" for the full license governing this code. --
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY warmup2_tb IS
END ENTITY;

ARCHITECTURE sim OF warmup2_tb IS

	constant CLK_PERIOD : time := 2 ns;

	signal sim_clk 	: std_logic := '0';
	signal sim_rst		: std_logic;
	signal sim_bts		: std_logic_vector(6 downto 0);
	signal sim_sws		: std_logic_vector(7 downto 0);
	signal sim_led		: std_logic_vector(7 downto 0);
	signal sim_irq		: std_logic;

	COMPONENT lt16soc_top IS
		generic(
			programfilename : string := "programs/assignment2codeisr.ram"
		);
		port(
			clk		: in  std_logic;
			rst		: in std_logic;
			-- external LEDs
			led		: out std_logic_vector(7 downto 0);
			-- external swtich buttons
			btn		: in std_logic_vector(6 downto 0);
			sw			: in std_logic_vector(7 downto 0);
			-- timer
			timer_irq 	: out std_logic
		);
	END COMPONENT;

BEGIN

	dut: lt16soc_top port map(
		clk=>sim_clk,
		rst=>sim_rst,
		led=>sim_led,
		btn=>sim_bts,
		sw=>sim_sws,
		timer_irq=>sim_irq
	);

	clk_gen: process
	begin
		sim_clk	<= not sim_clk;
		wait for CLK_PERIOD/2;
	end process clk_gen;

	stimuli: process
	begin
		sim_rst	<= '0';
		wait for CLK_PERIOD;
		sim_rst	<= '1';
		sim_bts	<= "0000001";
		sim_sws	<= "00000001";
		
		wait for CLK_PERIOD*1000;
		sim_bts	<= "0000010";
		sim_sws	<= "00000010";
		
		wait for CLK_PERIOD*1000;
		sim_bts	<= "0000011";
		sim_sws	<= "00000011";
		
		wait for CLK_PERIOD*1000;
		sim_bts	<= "1000000";
		sim_sws	<= "10000000";
	
		wait for 20000*CLK_PERIOD;
		assert false report "Simulation terminated!" severity failure;
	end process stimuli;


END ARCHITECTURE;
