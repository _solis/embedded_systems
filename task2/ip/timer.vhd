library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use work.lt16x32_global.all;
use work.wishbone.all;
use work.config.all;

entity wb_timer is
	generic(
		-- For simulating by itself, default values have to be given
		memaddr	:	generic_addr_type := CFG_BADR_TIMER;
		addrmask	:	generic_mask_type := CFG_MADR_TIMER
	);
	port(
		clk		: in  std_logic;
		rst		: in  std_logic;
		timer_irq : out std_logic;
		wslvi	:	in	wb_slv_in_type;
		wslvo	:	out	wb_slv_out_type
	);
end wb_timer;

architecture Behavioral of wb_timer is
	signal ack	: std_logic;
	signal timer_count : std_logic_vector(31 downto 0);
	signal max_cnt 	: std_logic_vector(31 downto 0);
	signal control 	: std_logic_vector(31 downto 0);
	signal temp: std_logic_vector(3 downto 0);
	-- Bits
	constant BIT_ENB    : integer := 0; -- 0/1 disable/enable
	constant BIT_REP    : integer := 1; -- 0/1 no/yes repeat
	constant BIT_RST    : integer := 2; -- 0/1
	constant BIT_IRQ    : integer := 3; -- event bit
    
begin

	process(clk)
    begin
		if clk'event and clk = '1' then
			if rst = '1' then
				ack		<= '0';
				timer_irq <= '0';
				timer_count <= (others => '0');
				max_cnt <= (others => '0');
				control <= (others => '0');
				temp <= (others => '0');
			else -- rst = '0'
				if wslvi.stb = '1' and wslvi.cyc = '1' then
					if ack = '0' then
						ack	<= '1';
					else
						ack	<= '0';
					end if;
					if wslvi.we='1' then
						-- check we're writing to the max_cnt register
		            if wslvi.adr(2) = '0' then
							max_cnt <= dec_wb_dat(wslvi.sel,wslvi.dat);
						elsif wslvi.adr(2) = '1' then
						 -- else, it's a write to the control
							temp(2 downto 0) <= dec_wb_dat(wslvi.sel,wslvi.dat)(2 downto 0);
							-- Configures control register
							control(31 downto 3)  <= (others => '0');
							control(BIT_ENB) <= temp(BIT_ENB);
							control(BIT_REP) <= temp(BIT_REP);
							control(BIT_RST) <= temp(BIT_RST);
						end if;
					else
					-- check we're reading the max_cnt register
						if wslvi.adr(2) = '0' then
							wslvo.dat <= max_cnt;
						elsif wslvi.adr(2) = '1' then
							-- else, it's a read to the control
							wslvo.dat(31 downto 3)  <= (others => '0');
							-- [2]: Reset, [1]: Repeat, [0]: Enable
							wslvo.dat(BIT_RST)  <= '0'; -- Always read 0
							wslvo.dat(BIT_REP)  <= control(BIT_REP);
							wslvo.dat(BIT_ENB)  <= control(BIT_ENB);
						end if;
					end if;
				else
					ack <= '0';
				end if;
				-- TIMER LOGIC
				-- Pulse interrupt
				if (temp(BIT_IRQ) = '1') then
					temp(BIT_IRQ) <= '0';
					timer_irq <= '0';
				end if;
				-- Enable: Setting this to '1' enables incrementing. It is reset to '0' 
				-- 	when the target count is reached, unless the "repeat" flag was set. Initially '0'.
				--		Clearing this flag stops the counter, but does not erase it.
				if (temp(BIT_ENB) = '1') then
					-- INCREMENT
					if (timer_count < max_cnt) then
						timer_count <= timer_count + 1;
						if (timer_count = (max_cnt-1)) then
							timer_irq <= '1';
							temp(BIT_IRQ) <= '1';
						end if;
					end if;
					if ((timer_count = max_cnt)) then
						-- Repeat: Setting this to '1' will make the counter start again after an 
						-- 	event was produced. This flag is not reset in this case. Initially '0'.
						if (temp(BIT_REP) = '1') then
							timer_count <= (others => '0');
						else
							control(BIT_ENB) <= '0';
							temp(BIT_ENB) <= '0';
						end if;
					end if;
				end if;
				-- Reset: Writing a '1' resets the counter value to '0'. It always reads as '0'.
				if (temp(BIT_RST) = '1') then
					timer_count <= (others => '0');
					control(BIT_RST) <= '0';
					temp(BIT_RST) <= '0';
				end if;
			end if;
		end if;
	end process;	    
	wslvo.ack	<= ack;
	wslvo.wbcfg	<= wb_membar(memaddr, addrmask);	
end Behavioral;

