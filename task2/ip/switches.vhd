library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.lt16x32_global.all;
use work.wishbone.all;
use work.config.all;

entity wb_switches is
	generic(
		-- For simulating by itself, default values have to be given
		memaddr		:	generic_addr_type := CFG_BADR_SWITCHES;
		addrmask	:	generic_mask_type := CFG_MADR_SWITCHES
	);
	port(
		clk		: in  std_logic;
		rst		: in  std_logic;
		bts		: in std_logic_vector(6 downto 0);
		sws		: in std_logic_vector(7 downto 0);
		wslvi	:	in	wb_slv_in_type;
		wslvo	:	out	wb_slv_out_type
	);
end wb_switches;

architecture Behavioral of wb_switches is

	signal status_bts     : std_logic_vector(6 downto 0);
	signal status_sws     : std_logic_vector(7 downto 0);
	signal ack	: std_logic;

begin

	process(clk)
	begin
		if clk'event and clk='1' then
			if rst = '1' then
				ack		<= '0';
				status_bts <= "0000000";
				status_sws <= x"00";
			else
				-- Always assign status
				status_bts <= bts;
				status_sws <= sws;
				if wslvi.stb = '1' and wslvi.cyc = '1' then
					if wslvi.we='0' then
						-- BE: [31:24] [23] [22:17] [15:8] [7:0]
						--       SW      0     BT     0      0
						wslvo.dat(31 downto 24) <= status_sws;
						wslvo.dat(23) <= '0';
						wslvo.dat(22 downto 16) <= status_bts;
						wslvo.dat(15 downto 0) <= (others => '0');
					end if;
					if ack = '0' then
						ack	<= '1';
					else
						ack	<= '0';
					end if;
				else
					ack <= '0';
				end if;
			end if;
		end if;
		wslvo.ack	<= ack;
		wslvo.wbcfg	<= wb_membar(memaddr, addrmask);
	end process;
end Behavioral;
