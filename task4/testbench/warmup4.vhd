-- See the file "LICENSE" for the full license governing this code. --
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE work.ps2_utils.all;

ENTITY warmup4_tb IS
END ENTITY;

ARCHITECTURE sim OF warmup4_tb IS
	constant MAIN_FREQ		: real := 200.000E6;
	constant PS2_FREQ			: real := 100.000E6;
	constant CLK_PERIOD 		: time := 1 sec / MAIN_FREQ;
	constant PS2_CLK_PERIOD : time := 1 sec / PS2_FREQ;
	
	signal sim_clk 	: std_logic := '0';
	signal sim_rst		: std_logic;
	signal sim_bts		: std_logic_vector(6 downto 0);
	signal sim_sws		: std_logic_vector(7 downto 0);
	signal sim_led		: std_logic_vector(7 downto 0);
	signal sim_irq		: std_logic;
	signal sim_dataLCD		: std_logic_vector(7 downto 0);
	signal sim_busyLCD		: std_logic;
	signal sim_enableLCD		: std_logic;
	signal sim_rsLCD			: std_logic;
	signal sim_rwLCD			: std_logic;
	signal sim_rx_data		: std_logic_vector(7 downto 0);
	signal sim_rx_data_ready : std_logic;
	signal sim_rx_parity_err : std_logic;
	signal sim_tx_ack_err    : std_logic;		
	signal sim_ps2_clk	: std_logic := 'H';
	signal sim_ps2_data	: std_logic := 'H';
	signal var 				: std_logic_vector(31 downto 0); -- temporary variable

	COMPONENT lt16soc_top IS
		generic(
			programfilename : string := "programs/assignment4code.ram"
		);
		port(
			-- clock signal
			clk		: in  std_logic;
			-- external reset button
			rst		: in std_logic;
			-- external LEDs
			led		: out std_logic_vector(7 downto 0);
			-- external swtich buttons
			btn		: in std_logic_vector(6 downto 0);
			sw			: in std_logic_vector(7 downto 0);
			-- Timer
			timer_irq : out std_logic;
			-- LCD
			dataLCD		: inout std_logic_vector(7 downto 0);
			busyLCD     : out std_logic;
			enableLCD	: out std_logic;
			rsLCD		: out std_logic;
			rwLCD		: out std_logic;
			-- RX
			rx_data			: out std_logic_vector(7 downto 0);
			rx_data_ready 	: out std_logic;
			rx_parity_err	: out std_logic;
			-- TX
			tx_ack_err		: out std_logic;
			-- Keyboard
			ps2_keyb_clk	: inout std_logic;
			ps2_keyb_data	: inout std_logic
		);
	END COMPONENT;

BEGIN
	clk_gen: process
	begin
		sim_clk	<= not sim_clk;
		wait for CLK_PERIOD/2;
	end process clk_gen;
	
	dut: lt16soc_top port map(
		clk=>sim_clk,
		rst=>sim_rst,
		led=>sim_led,
		btn=>sim_bts,
		sw=>sim_sws,
		timer_irq=>sim_irq,
		dataLCD=>sim_dataLCD,
		busyLCD=>sim_busyLCD,
		enableLCD=>sim_enableLCD,
		rsLCD=>sim_rsLCD,
		rwLCD=>sim_rwLCD,
		rx_data			=> sim_rx_data,
		rx_data_ready	=> sim_rx_data_ready,
		rx_parity_err 	=> sim_rx_parity_err,
		tx_ack_err		=> sim_tx_ack_err,
		ps2_keyb_clk	=> sim_ps2_clk,
		ps2_keyb_data	=> sim_ps2_data
	);


	stimuli: process
	begin
		sim_rst	<= '0';
		var <= x"FACEFACE";
		-- Start
		wait for CLK_PERIOD;
			sim_rst	<= '1';
			-- clk/data are high due to pull up
			sim_ps2_clk <= '1';
			sim_ps2_data <= '1';
-----------------------------------------------------------
-- DEVICE TO HOST, 
-- 	Device Sends: DELETE key [0x71 scan code]/[0x7F ASCII]
--			E0,71(key press) -> E0,F0,71 (key release)
-- 	Sequence of TB events:
--			Send DATA on HIGH clock cycle
--			Toggle clock, Wait a clock cycle
--			Repeat
-----------------------------------------------------------
-------------
-- KEY PRESS
-------------
-- Send Extended key, E0		
		wait for CLK_PERIOD*20;
			sim_ps2_clk <= '1';
			-- start bit (clock should already be high here from previous)
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D0:D3], 0000
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D4:D7], 0111
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;

		-- parity
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		-- stop
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			
-- Send key data, 71
		wait for CLK_PERIOD;
			-- start
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D0:D3], 1000
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D4:D7], 1110
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;

		-- parity
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		-- stop
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		
--------------
-- KEY RELEASE
--------------
-- Send Extended key for key release
		wait for CLK_PERIOD*10;
			-- start
			sim_ps2_data <= '0';
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D0:D3], 0000
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D4:D7], 0111
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;

		-- parity
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		-- stop
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			
-- Send break, F0
		wait for CLK_PERIOD;
			-- start
			sim_ps2_data <= '0';
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D0:D3], 0000
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D4:D7], 1111
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;

		-- parity
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		-- stop
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		
-- Send DATA again
		wait for CLK_PERIOD;
			-- start
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D0:D3], 1000
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D4:D7], 1110
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;

		-- parity
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
		-- stop
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;

-- Lines go back to pull-up high
		wait for CLK_PERIOD;
			sim_ps2_clk  <= '1';
			sim_ps2_data <= '1';
			
		wait for CLK_PERIOD;
		wait for CLK_PERIOD*10;
		-- END
		wait for CLK_PERIOD;		
			var <= x"DEADDEAD";
		wait;
	end process stimuli;


END ARCHITECTURE;
