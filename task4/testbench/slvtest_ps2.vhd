-- See the file "LICENSE" for the full license governing this code. --
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

LIBRARY work;
USE work.lt16soc_peripherals.ALL;
USE work.wishbone.ALL;
USE work.wb_tp.ALL;
USE work.config.ALL;
USE work.ps2_utils.all;

ENTITY slvtest_ps2 IS
END ENTITY;

ARCHITECTURE sim OF slvtest_ps2 IS
	constant MAIN_FREQ		: real := 200.000E6;
	constant PS2_FREQ			: real := 100.000E6;
	constant CLK_PERIOD 		: time := 1 sec / MAIN_FREQ;
	constant PS2_CLK_PERIOD : time := 1 sec / PS2_FREQ;
	constant STABLE_CLK		: time := CLK_PERIOD*3;
	signal sim_clk 		: std_logic := '0';
	signal sim_rst			: std_logic;
	signal sim_rx_data	: std_logic_vector(7 downto 0);
	signal sim_rx_data_ready : std_logic;
	signal sim_rx_parity_err : std_logic;
	signal sim_tx_ack_err    : std_logic;		
	signal sim_ps2_clk	: std_logic := 'H';
	signal sim_ps2_data	: std_logic := 'H';
   signal slvi				: wb_slv_in_type;
	signal slvo				: wb_slv_out_type;
	signal var 				: std_logic_vector(31 downto 0); -- temporary variable
BEGIN
	clk_gen: process
	begin
		sim_clk	<= not sim_clk;
		wait for CLK_PERIOD/2;
	end process clk_gen;
	
	SIM_SLV: wb_ps2
		generic map(
			memaddr		=> CFG_BADR_PS2,
			addrmask		=> CFG_MADR_PS2
		)
		port map(
			clk				=> sim_clk,
			rst				=> sim_rst,
			rx_data			=> sim_rx_data,
			rx_data_ready	=> sim_rx_data_ready,
			rx_parity_err 	=> sim_rx_parity_err,
			tx_ack_err		=> sim_tx_ack_err,
			ps2_keyb_clk	=> sim_ps2_clk,
			ps2_keyb_data	=> sim_ps2_data,
			wslvi			=> slvi,
			wslvo			=> slvo
		);

	stimuli: process
	begin
		sim_rst	<= '1';
		var <= x"FACEFACE";
		
		-- Start
		wait for CLK_PERIOD;
			sim_rst	<= '0';
			var <= x"00000000";
			-- clk/data are high due to pull up
			sim_ps2_clk  <= 'Z';
			sim_ps2_data <= 'Z';
-----------------------------------------------------------
-- DEVICE TO HOST, 
-- 	Device Sends: DELETE key [0x71 scan code]/[0x7F ASCII]
--			E0,71(key press) -> E0,F0,71 (key release)
-- 	Sequence of TB events:
--			Send DATA on HIGH clock cycle
--			Toggle clock, Wait a clock cycle
--			Repeat
-----------------------------------------------------------
-------------
-- KEY PRESS
-------------
-- Send Extended key, E0
		wait for PS2_CLK_PERIOD*10;
			var <= x"00000000";
			sim_ps2_clk <= '1';
		wait for STABLE_CLK;
			-- start bit (clock should already be high here from previous)
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D0:D3], 0000
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D4:D7], 0111
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;

		-- parity
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		-- stop
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			
-- Send key data, 71
		wait for STABLE_CLK;
			-- start
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D0:D3], 1000
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D4:D7], 1110
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;

		-- parity
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		-- stop
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		-- Simulates the interrupt for CPU
		if (sim_rx_data_ready = '0') then
			wait on sim_rx_data_ready until sim_rx_data_ready = '1';
		end if;
		var <= x"00000000";
		wait for STABLE_CLK;
			generate_sync_wb_single_read(slvi,slvo,sim_clk,var,0,"10");
--------------
-- KEY RELEASE
--------------
-- Send Extended key for key release
		wait for STABLE_CLK;
			-- start
			sim_ps2_data <= '0';
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D0:D3], 0000
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D4:D7], 0111
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;

		-- parity
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		-- stop
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			
-- Send break, F0
		wait for STABLE_CLK;
			-- start
			sim_ps2_data <= '0';
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D0:D3], 0000
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D4:D7], 1111
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for CLK_PERIOD;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;

		-- parity
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		-- stop
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		
-- Send DATA again
		wait for STABLE_CLK;
			-- start
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D0:D3], 1000
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		
		-- [D4:D7], 1110
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '0';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;

		-- parity
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
		-- stop
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;
			sim_ps2_data <= '1';
		wait for STABLE_CLK;
			sim_ps2_clk <= not sim_ps2_clk;

-- Lines go back to pull-up high
		wait for STABLE_CLK;
			sim_ps2_clk  <= '1';
			sim_ps2_data <= '1';
			
		wait for STABLE_CLK;
		wait for CLK_PERIOD*20;
		-- END
		wait for CLK_PERIOD;		
			var <= x"DEADDEAD";
		wait;
	end process stimuli;

END ARCHITECTURE;

