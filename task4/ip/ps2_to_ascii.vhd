library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


-- ps2 scan codes to ascii characters
entity ps2_to_ascii is
port(
	clk			: in std_logic;
	rst			: in std_logic;
	new_ps2		: in std_logic;
	ps2_code		: in std_logic_vector(7 downto 0);
	ascii_code	: out std_logic_vector(7 downto 0);
	new_ascii	: out std_logic
	);
end ps2_to_ascii;
architecture logic of ps2_to_ascii is
	signal prev_ps2_code_new 	: std_logic := '0';
	signal break 					: std_logic := '0';
	signal extended 				: std_logic := '0';
	signal caps_lock				: std_logic := '0';
	signal ctrl_rite				: std_logic := '0';
	signal ctrl_left				: std_logic := '0';
	signal shift_rite				: std_logic := '0';
	signal shift_left				: std_logic := '0';
	signal ascii 					: std_logic_vector(7 downto 0) := (others => '0');
	type PS2_TO_ASCII_STATES is(READY, PS2TOASCII, OUTPUT);
   signal STATE : PS2_TO_ASCII_STATES;
begin

process(clk)
begin
	if(clk'event and clk = '1') then
		if rst = '1' then
			ascii_code <= (others => '0');
		else
		case STATE is
			when READY =>
				new_ascii 	<= '0';
				if(new_ps2 = '1') then
					ascii_code 	<= (others => '0');
					-- check for break code
					if(ps2_code = x"F0") then
						break <= '1';
						STATE <= READY;
					-- check for extended code
					elsif(ps2_code = x"E0") then
						extended <= '1';
						STATE <= READY;
					else
						ascii(7) <= '1';
						STATE <= PS2TOASCII;
					end if;
				end if;
			--translate state: translate ps2 code to ascii value
			when PS2TOASCII =>
				-- Reset flags, will be low on cycle
				break 	<= '0';
				extended <= '0';

				--handle codes for control, shift, and caps lock
				case ps2_code is
					-- caps lock
					when x"58" => 
						if(break = '0') then
							caps_lock <= not caps_lock;
						end if;
					-- control keys
					when x"14" =>
						if(extended = '1') then
							ctrl_rite <= not break;
						else
							ctrl_left <= not break;
						end if;
					-- shift keys
					when x"12" =>
						shift_left <= not break;
					when x"59" =>
						shift_rite <= not break;
					when others => null;
				end case;

				--translate characters that do not depend on shift, or caps lock
				case ps2_code is
					when x"29" => ascii <= x"20"; --space
					when x"66" => ascii <= x"08"; --backspace (bs control code)
					when x"0d" => ascii <= x"09"; --tab (ht control code)
					when x"5a" => ascii <= x"0d"; --enter (cr control code)
					when x"76" => ascii <= x"1b"; --escape (esc control code)
					when x"71" => 
					if(extended = '1') then
						ascii <= x"7f";             --delete
					end if;
					when x"6b" =>
					if(extended = '1') then
						ascii <= x"11";				-- left arrow
					end if;					
					when x"72" =>
					if(extended = '1') then
						ascii <= x"12";				-- down arrow
					end if;					
					when x"74" =>
					if(extended = '1') then
						ascii <= x"13";				-- right arrow
					end if;					
					when x"75" =>
					if(extended = '1') then
						ascii <= x"14";				-- up arrow
					end if;				
					when others => null;
				end case;

				--translate letters (these depend on both shift and caps lock)
				if((shift_rite = '0' and shift_left = '0' and caps_lock = '0') or
				((shift_rite = '1' or shift_left = '1') and caps_lock = '1')) then 
				--letter is lowercase
					case ps2_code is              
						when x"1c" => ascii <= x"61"; --a
						when x"32" => ascii <= x"62"; --b
						when x"21" => ascii <= x"63"; --c
						when x"23" => ascii <= x"64"; --d
						when x"24" => ascii <= x"65"; --e
						when x"2b" => ascii <= x"66"; --f
						when x"34" => ascii <= x"67"; --g
						when x"33" => ascii <= x"68"; --h
						when x"43" => ascii <= x"69"; --i
						when x"3b" => ascii <= x"6a"; --j
						when x"42" => ascii <= x"6b"; --k
						when x"4b" => ascii <= x"6c"; --l
						when x"3a" => ascii <= x"6d"; --m
						when x"31" => ascii <= x"6e"; --n
						when x"44" => ascii <= x"6f"; --o
						when x"4d" => ascii <= x"70"; --p
						when x"15" => ascii <= x"71"; --q
						when x"2d" => ascii <= x"72"; --r
						when x"1b" => ascii <= x"73"; --s
						when x"2c" => ascii <= x"74"; --t
						when x"3c" => ascii <= x"75"; --u
						when x"2a" => ascii <= x"76"; --v
						when x"1d" => ascii <= x"77"; --w
						when x"22" => ascii <= x"78"; --x
						when x"35" => ascii <= x"79"; --y
						when x"1a" => ascii <= x"7a"; --z
						when others => null;
					end case;
				else                                     
				--letter is uppercase
					case ps2_code is            
						when x"1c" => ascii <= x"41"; --a
						when x"32" => ascii <= x"42"; --b
						when x"21" => ascii <= x"43"; --c
						when x"23" => ascii <= x"44"; --d
						when x"24" => ascii <= x"45"; --e
						when x"2b" => ascii <= x"46"; --f
						when x"34" => ascii <= x"47"; --g
						when x"33" => ascii <= x"48"; --h
						when x"43" => ascii <= x"49"; --i
						when x"3b" => ascii <= x"4a"; --j
						when x"42" => ascii <= x"4b"; --k
						when x"4b" => ascii <= x"4c"; --l
						when x"3a" => ascii <= x"4d"; --m
						when x"31" => ascii <= x"4e"; --n
						when x"44" => ascii <= x"4f"; --o
						when x"4d" => ascii <= x"50"; --p
						when x"15" => ascii <= x"51"; --q
						when x"2d" => ascii <= x"52"; --r
						when x"1b" => ascii <= x"53"; --s
						when x"2c" => ascii <= x"54"; --t
						when x"3c" => ascii <= x"55"; --u
						when x"2a" => ascii <= x"56"; --v
						when x"1d" => ascii <= x"57"; --w
						when x"22" => ascii <= x"58"; --x
						when x"35" => ascii <= x"59"; --y
						when x"1a" => ascii <= x"5a"; --z
						when others => null;
					end case;
				end if;

				--translate numbers and symbols (these depend on shift but not caps lock)
				if(shift_left = '1' or shift_rite = '1') then 
				--key's secondary character is desired
					case ps2_code is              
						when x"16" => ascii <= x"21"; --!
						when x"52" => ascii <= x"22"; --"
						when x"26" => ascii <= x"23"; --#
						when x"25" => ascii <= x"24"; --$
						when x"2e" => ascii <= x"25"; --%
						when x"3d" => ascii <= x"26"; --&              
						when x"46" => ascii <= x"28"; --(
						when x"45" => ascii <= x"29"; --)
						when x"3e" => ascii <= x"2a"; --*
						when x"55" => ascii <= x"2b"; --+
						when x"4c" => ascii <= x"3a"; --:
						when x"41" => ascii <= x"3c"; --<
						when x"49" => ascii <= x"3e"; -->
						when x"4a" => ascii <= x"3f"; --?
						when x"1e" => ascii <= x"40"; --@
						when x"36" => ascii <= x"5e"; --^
						when x"4e" => ascii <= x"5f"; --_
						when x"54" => ascii <= x"7b"; --{
						when x"5d" => ascii <= x"7c"; --|
						when x"5b" => ascii <= x"7d"; --}
						when x"0e" => ascii <= x"7e"; --~
						when others => null;
					end case;
				else                                     
				--key's primary character is desired
					case ps2_code is  
						when x"45" => ascii <= x"30"; --0
						when x"16" => ascii <= x"31"; --1
						when x"1e" => ascii <= x"32"; --2
						when x"26" => ascii <= x"33"; --3
						when x"25" => ascii <= x"34"; --4
						when x"2e" => ascii <= x"35"; --5
						when x"36" => ascii <= x"36"; --6
						when x"3d" => ascii <= x"37"; --7
						when x"3e" => ascii <= x"38"; --8
						when x"46" => ascii <= x"39"; --9
						when x"52" => ascii <= x"27"; --'
						when x"41" => ascii <= x"2c"; --,
						when x"4e" => ascii <= x"2d"; ---
						when x"49" => ascii <= x"2e"; --.
						when x"4a" => ascii <= x"2f"; --/
						when x"4c" => ascii <= x"3b"; --;
						when x"55" => ascii <= x"3d"; --=
						when x"54" => ascii <= x"5b"; --[
						when x"5d" => ascii <= x"5c"; --\
						when x"5b" => ascii <= x"5d"; --]
						when x"0e" => ascii <= x"60"; --`
						when others => null;
					end case;
				end if;
				-- code is make
				if(break = '0') then
					STATE <= OUTPUT;
				-- code is break
				else
					STATE <= READY;
				end if;

			--output state: verify the code is valid and output the ascii value
			when OUTPUT =>
				-- code is ascii value
				if(ascii(7) = '0') then
					ascii_code <= ascii;
					new_ascii  <= '1';
				end if;
				STATE <= READY;
			end case;
		end if;
		end if;
	end process;
end logic;
