library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

-- Stabilizes input data
entity stabilizer is
  port(
    clk   : in  std_logic;  --input clock
    input : in  std_logic;  --input signal to synchronize
    result  : out std_logic);
end stabilizer;

architecture logic of stabilizer is
  signal ffs         : std_logic_vector(1 downto 0);
  signal counter_set : std_logic;
  signal counter     : std_logic_vector(15 downto 0) := (others => '0');
begin
	
	counter_set <= ffs(0) xor ffs(1);   --determine when to start/reset counter
  
	process(clk)
	begin
		if(clk'event and clk = '1') then
			ffs(0) <= input;
			ffs(1) <= ffs(0);
			-- If input changes, we reset our counter
			if(counter_set = '1') then
				counter <= (others => '0');
			-- stable time is not yet set
			elsif(counter(11) = '0') then 
				counter <= counter + 1;
			else 
			-- input is stable
				result <= ffs(1);
			end if;    
		end if;
	end process;
end logic;
