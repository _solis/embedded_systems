library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--------------------------------------------------------------------
-- DECLERATIONS
--------------------------------------------------------------------
package ps2_utils is
-- parity generator
function parity_generate (
	signal input_bits : std_logic_vector(7 downto 0)	--input data
) return std_logic;
end ps2_utils;
--------------------------------------------------------------------
-- DEFINITIONS
--------------------------------------------------------------------
package body ps2_utils is
-- PARITY GENERATOR
function parity_generate (
	signal input_bits : std_logic_vector(7 downto 0)	--input data
) return std_logic is
	variable	parity_calc	: std_logic_vector(8 downto 0);	--intermediate results
	begin
		--parity calculation logic
		parity_calc(0) := '1';										--set first for odd parity
		parity_logic: for i in 0 to 7 loop
		parity_calc(i+1) := parity_calc(i) xor input_bits(i);	--xor each result with the next input bit
	end loop;
	return parity_calc(8); --output the final result
end function;
end ps2_utils;

 

              
              
              
             
