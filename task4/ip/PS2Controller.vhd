-- See the file "LICENSE" for the full license governing this code. --

LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;
USE work.lt16x32_global.all;
USE work.wishbone.all;
USE work.config.all;
USE work.ps2_utils.all;

ENTITY wb_ps2 IS
	generic(
		memaddr		:	generic_addr_type := CFG_BADR_PS2;
		addrmask		:	generic_mask_type := CFG_MADR_PS2
	);
	port(
		clk			: in std_logic;
		rst			: in std_logic;
		-- RX
		rx_data			: out std_logic_vector(7 downto 0);
		rx_data_ready 	: out std_logic;
		rx_parity_err	: out std_logic;
		-- TX
		tx_ack_err		: out std_logic;
		-- Keyboard
		ps2_keyb_clk	: inout std_logic;
		ps2_keyb_data	: inout std_logic;
		-- Bus
		wslvi			: in wb_slv_in_type;
		wslvo			: out wb_slv_out_type
	);
END ENTITY;

ARCHITECTURE Behavioral OF wb_ps2 IS
	-- Bus
	signal ws_ack		: std_logic := '0';
	-- RX
	signal ascii_out		: std_logic	:= '0';
	signal new_scan_code	: std_logic := '0';
	signal ascii_data		: std_logic_vector(7 downto 0) := (others => '0');
	signal parity_error	: std_logic := '1';
	signal stop_error		: std_logic := '1';
	signal start_error	: std_logic := '1';
	signal rx_err			: std_logic := '1';
	signal prev_code		: std_logic_vector(7 downto 0) := (others => '0');
	-- TX
	signal tx_data		: std_logic_vector(7 downto 0) := (others => '0');
	signal tx_err		: std_logic := '0';
	constant TX_EN		: integer := 8;
	-- Others
	signal hold_reg		: std_logic_vector(10 downto 0) := (others => '0');
	signal timer  			: integer := 0;
	signal bit_cnt			: integer := 0;
	signal sync_ffs     : std_logic_vector(1 downto 0);
	signal stable_ps2_clk : std_logic;
	signal ps2_prev_clk : std_logic;
	signal stable_ps2_data : std_logic;
	-- Controller states
	type CONTROLLER_STATES is(INIT, READY, TX_INIT, TX, TX_DONE);
	signal CTRL_STATE : CONTROLLER_STATES;
	-- delay
	constant us100    : integer := 10000; -- Use 5 for simulation, 10000 for implementation
	--constant us100		: integer := 6;
	-- bit fields
	constant BIT_STOP 	: integer := 10;
	constant BIT_START	: integer := 0;
	constant BIT_PARITY 	: integer := 9;
	-- synchronizer
	component stabilizer is
		port(
		clk    : in  std_logic;
		input  : in  std_logic;
		result : out std_logic);
	end component;
	-- ps2 to ascii
	component ps2_to_ascii is
	port(
		clk			: in std_logic;
		rst			: in std_logic;
		new_ps2		: in std_logic;
		ps2_code		: in std_logic_vector(7 downto 0);
		ascii_code	: out std_logic_vector(7 downto 0);
		new_ascii	: out std_logic
		);
	end component;
BEGIN	
  --synchronizer flip-flops
  process(clk)
  begin
    if(clk'event and clk = '1') then
      sync_ffs(0) <= ps2_keyb_clk;
      sync_ffs(1) <= ps2_keyb_data;
    end if;
  end process;
	-- sync PS/2 signals
	stabilizer_ps2_clk: stabilizer
		port map(clk => clk, input => sync_ffs(0), result => stable_ps2_clk);
	stabilizer_ps2_data: stabilizer
		port map(clk => clk, input => sync_ffs(1), result => stable_ps2_data);
	-- ps2 to ascii
	ps2_to_ascii_machine: ps2_to_ascii
		port map
		(
			clk 			=> clk,
			rst			=> rst,
			new_ps2 		=> new_scan_code,
			ps2_code 	=> hold_reg(8 downto 1),
			ascii_code 	=> ascii_data,
			new_ascii 	=> ascii_out
		);
process(clk)
	begin
		if clk'event and clk='1' then
			if rst = '1' then
				-- Bus
				ws_ack	<= '0';
				-- RX
				parity_error	<= '1';
				stop_error		<= '1';
				start_error		<= '1';
				new_scan_code	<= '0';
				rx_err			<= '1';
				prev_code		<= (others => '0');
				-- TX
				tx_data			<= (others => '0');
				tx_err			<= '0';
				-- Others
				hold_reg			<= (others => '0');
				timer				<= 0;
				bit_cnt			<= 0;
				-- Keyboard, inhibit comms until controller is ready
				ps2_keyb_clk	<= '0';
				ps2_keyb_data	<= '1';
				CTRL_STATE 		<= INIT;
			else
				ps2_prev_clk 	<= stable_ps2_clk;
			-- PS/2 CONTROLLER STATE MACHINE
				case CTRL_STATE is
					when INIT =>
						-- Bus
						ws_ack	<= '0';
						-- RX
						parity_error	<= '1';
						stop_error		<= '1';
						start_error		<= '1';
						new_scan_code	<= '0';
						rx_err			<= '1';
						prev_code		<= (others => '0');
						-- TX
						tx_data			<= (others => '0');
						tx_err			<= '0';
						-- Others
						hold_reg			<= (others => '0');
						timer				<= 0;
						bit_cnt			<= 0;
						-- Keyboard
						ps2_keyb_clk	<= 'Z';
						ps2_keyb_data	<= 'Z';
						CTRL_STATE 		<= READY;
					when READY =>
						if wslvi.stb = '1' and wslvi.cyc = '1' then
							ws_ack <= ws_ack xor '1';
							if wslvi.we = '1' then
								tx_data <= dec_wb_dat(wslvi.sel,wslvi.dat)(7 downto 0);
								-- Host wants to transmit, host supplies PS/2 value
								-- controller then adds parity, start, and stop bits
								timer 	<= us100;
								-- [START][D0:D7][PARITY][STOP] -> Original
								-- [STOP][PARITY][D7:D0][START] -> What's being sent
								hold_reg(0) 	<= '0';	-- start
								hold_reg(10)	<= '1';	-- stop
								tx_err 		<= '0';
								CTRL_STATE 	<= TX_INIT;
							else -- not used, wslvi.we = '0' (read)
							
							end if; -- wslvi.we = '1'
						else
							ws_ack 			<= '0';
							ps2_keyb_clk 	<= 'Z';
							ps2_keyb_data 	<= 'Z';
							new_scan_code	<= '0';
							-- Keep setting internal signals until we have data_ready and bit_cnt != 11
							-- (bit_cnt == 10 signals state machine to set the actual signals)
							if(bit_cnt <= 10 and ascii_out = '0') then
								stop_error		<= '1';
								start_error		<= '1';
								parity_error	<= '1';
								rx_err			<= '1';
							end if;
							-- we only read in falling edges
							if(ps2_prev_clk = '1' and stable_ps2_clk = '0') then
								-- recieve data
								if(bit_cnt < 11) then
									-- shift data in, [STOP][PARITY][D7:D0][START]
									hold_reg(bit_cnt)  <= stable_ps2_data;
									bit_cnt <= bit_cnt + 1;
									timer   <= 0;
									-- internal bit checks
									if(bit_cnt = BIT_STOP) then
										-- Parity check, first generates parity and xors with parity provided
										parity_error <= parity_generate(hold_reg(8 downto 1)) xor hold_reg(BIT_PARITY);
										-- Start/Stop bit check
										start_error <= hold_reg(BIT_START);
										stop_error  <= not stable_ps2_data;
									end if;
								end if; -- if(ps2_keyb_clk = '0') then
							else
								timer <= timer + 1;
							end if; -- if(ps2_keyb_clk'event and ps2_keyb_clk = '0') then
							-- transaction is done
							if(bit_cnt = 11) then --if(timer < (us100/2)) then
								if(start_error = '1' or parity_error = '1' or stop_error = '1') then
									rx_err 		<= '1';
									new_scan_code <= '0';
								else
									-- Tell PS2 to ASCII machine to translate
									new_scan_code 	<= '1';
									rx_err			<= '0';
									timer 			<= 0;
								end if; -- if(rx_err = '1') then
								bit_cnt <= 0;
							end if; -- if(bit_cnt = 11) then
							CTRL_STATE <= READY;
						end if; --wslvi.stb = '1' and wslvi.cyc = '1' then
					when TX_INIT =>
						-- Inhibit the device by setting clk low for 100us
						-- after this, we can transmit data
						if(timer > 0) then -- set in READY if we want to TX
							timer 			<= timer - 1;
							ps2_keyb_clk 	<= '0';
							ps2_keyb_data 	<= 'Z';
							CTRL_STATE 		<= TX_INIT;
						else
							ps2_keyb_clk  <= 'Z';
							ps2_keyb_data <= hold_reg(bit_cnt);	-- start bit
							bit_cnt <= bit_cnt + 1;
							-- Placed here because when we read the wishbone, tx_data is not set until the clock after
							-- If placed in the wishbone read, hold_reg will containt the incorrect values
							hold_reg(9) 			<= parity_generate(tx_data);
							hold_reg(8 downto 1) <= tx_data; -- data
							CTRL_STATE <= TX;
						end if;
					when TX =>
						-- set ps2 clock as host input
						-- device sets clock
						ps2_keyb_clk 	<= 'Z';
						-- shift data on rising edge and send on falling
						if(ps2_prev_clk = '1' and stable_ps2_clk = '0') then
							-- send data, start bit already sent in TX_INIT
							hold_reg <= '0' & hold_reg(10 downto 1);
							bit_cnt <= bit_cnt + 1;
						else
							ps2_keyb_data <= 'Z';
						end if;
						if(bit_cnt < 10) then
							ps2_keyb_data <= hold_reg(0);
						else
							ps2_keyb_data <= 'Z';
						end if;
						if(bit_cnt = 11) then
							ps2_keyb_data <= 'Z';
							tx_err <= stable_ps2_data;-- device sends acknowledgment, which is '0'
							CTRL_STATE 	<= TX_DONE;
						else
							CTRL_STATE <= TX;
						end if;
					when TX_DONE =>
						ps2_keyb_clk  <= 'Z';
						ps2_keyb_data <= 'Z';
						bit_cnt <= 0;
						-- Check if device has released the bus
						if(ps2_prev_clk = '1' and stable_ps2_clk = '1' and stable_ps2_data = '1') then
							tx_data  <= (others => '0');
							hold_reg <= (others => '0');
							CTRL_STATE <= READY;
						else
							CTRL_STATE <= TX_DONE;							
						end if;
				end case;
			end if; -- if rst = '1' then
		end if; -- if clk'event and clk='1' then
	end process;
	wslvo.dat(31 downto 11) <= (others => '0');
	wslvo.dat(10) 	<= tx_err; -- tx ack
	wslvo.dat(9) 	<= rx_err; -- general rx error, start, stop, parity bit
	wslvo.dat(8) 	<= ascii_out; -- data ready
	wslvo.dat(7 downto 0) <= ascii_data;
	wslvo.ack	<= ws_ack;
	wslvo.wbcfg	<= wb_membar(memaddr, addrmask);
	
	-- RX
	rx_data			<= ascii_data;
	rx_data_ready	<= ascii_out;
	rx_parity_err	<= rx_err;
	-- TX
	tx_ack_err 		<= tx_err;
END ARCHITECTURE;
